"""
Ce programme python est une transcription du modeleur 'CouvertHivernal.model3' présent dans le projet Gitlab. Ce
programme python est uniquement fonctionnel en étant lancé depuis la console python de QGIS.
"""

"""
Import des libraries QGIS

"""
from qgis.core import QgsProcessing
from qgis.core import QgsProcessingAlgorithm
from qgis.core import QgsProcessingMultiStepFeedback
from qgis.core import QgsProcessingParameterRasterLayer
from qgis.core import QgsProcessingParameterVectorLayer
from qgis.core import QgsProcessingParameterCrs
from qgis.core import QgsProcessingParameterFeatureSink
from qgis.core import QgsProcessingParameterVectorDestination
from qgis.core import QgsProcessingParameterRasterDestination
import processing

"""Création d'une classe 'Modle' qui détermine les paramètres propres au modeleur QGIS"""
class Modle(QgsProcessingAlgorithm):

    """ La fonction 'initAlgorithm' permet de définir les paramètres en entrée et en sortie du modèleur
    ou des ses fonctions intermédiaires"""
    def initAlgorithm(self, config=None):
        """Paramètres en entrée"""
        self.addParameter(QgsProcessingParameterRasterLayer('bande_pir', 'Bande PIR', defaultValue=None))
        self.addParameter(QgsProcessingParameterRasterLayer('bande_rouge', 'Bande Rouge', defaultValue=None))
        self.addParameter(QgsProcessingParameterVectorLayer('rpg', 'RPG', types=[QgsProcessing.TypeVectorAnyGeometry], defaultValue=None))
        self.addParameter(QgsProcessingParameterCrs('scr_souhait', 'SCR souhaité', defaultValue='EPSG:32631'))

        """Paramètre en sortie du modèle"""
        self.addParameter(QgsProcessingParameterFeatureSink('RpgEnrichi', 'RPG enrichi', type=QgsProcessing.TypeVectorAnyGeometry, createByDefault=True, supportsAppend=True, defaultValue=None))

        """Paramètres en sortie intermédiaire (contraint par OTB, qui ne peut pas utiliser des couches temporaires QGIS.
        Il convient alors d'enregister sur le disque local les couches en entrée et en sortie des fonctions OTB)"""
        self.addParameter(QgsProcessingParameterFeatureSink('RpgReprojet', 'RPG Reprojeté', type=QgsProcessing.TypeVectorAnyGeometry, createByDefault=True, supportsAppend=True, defaultValue=None))
        self.addParameter(QgsProcessingParameterVectorDestination('ZonalStatNdvi', 'Zonal stat NDVI', type=QgsProcessing.TypeVectorAnyGeometry, createByDefault=True, defaultValue=None))
        self.addParameter(QgsProcessingParameterRasterDestination('NdviDeLaZoneDtude', "NDVI de la zone d'étude", createByDefault=True, defaultValue=None))


    """La fonction 'processAlgorithm' exécute les fonctions de l'algorithme dans l'ordre définit lors de la construction
    du modeleur"""
    def processAlgorithm(self, parameters, context, model_feedback):
        
        """La variable 'feedback' permet de synchroniser les fonctions intermédiaires entre elles afin d'assurer la bonne
        gestion des entrées et sorties de chaque fonction intermédiaire"""
        feedback = QgsProcessingMultiStepFeedback(5, model_feedback)
        results = {}
        outputs = {}

        """Fonction : Reprojeter la couche RPG 
                Entrée : 'rpg', 'scr_souhait'
                Sortie : 'RpgReproject'
        Le but de cette fonction est de s'assurer de la bonne projection du RPG en entrée. Ce dernier doit avoir le même
        SCR que l'image NDVI construite précédemment."""

        alg_params = {
            'INPUT': parameters['rpg'],
            'OPERATION': '',
            'TARGET_CRS': parameters['scr_souhait'],
            'OUTPUT': parameters['RpgReprojet']
        }
        outputs['ReprojeterUneCouche'] = processing.run('native:reprojectlayer', alg_params, context=context, feedback=feedback, is_child_algorithm=True)
        results['RpgReprojet'] = outputs['ReprojeterUneCouche']['OUTPUT']

        """Faire passer de 0 à 1 le 'step' afin de faire comprendre à l'algorithme de passer à la fonction intermédiaire
        suivante"""
        feedback.setCurrentStep(1)
        if feedback.isCanceled():
            return {}

        """Fonction : Calculatrice Raster : 
        
        Cette fonction créée une image NDVI de la zone d'étude en prenant les bandes rouge(B4) et proche infrarouge (B8)
        de l'image Sentinel 2 de la zone d'étude. L'emprise spatiale de l'image ce fait en fonction de celle des bandes.
        La formule qui permet la création d'une image NDVI est la suivante  :  
        ' ( "Bande PIR@1"-"Bande Rouge@1")/("Bande PIR@1"+"Bande Rouge@1")'
        Cette fonction compte aussi un paramètre 'CRS' afin de s'assurer de la bonne projection de l'image en sortie pour
        limiter des potentiels erreur dans la suite de l'algorithme


                Entrées : 'bandes_pir', 'bandes_rouge', 'scr_souhait'
                Sortie : 'NdviDeLaZoneDtude'
                """
        alg_params = {
            'CELLSIZE': 0,
            'CRS': parameters['scr_souhait'],
            'EXPRESSION': ' ( "Bande PIR@1"-"Bande Rouge@1")/("Bande PIR@1"+"Bande Rouge@1")',
            'EXTENT': None,
            'LAYERS': [parameters['bande_pir'],parameters['bande_rouge']],
            'OUTPUT': parameters['NdviDeLaZoneDtude']
        }
        outputs['RasterCalculator'] = processing.run('qgis:rastercalculator', alg_params, context=context, feedback=feedback, is_child_algorithm=True)
        results['NdviDeLaZoneDtude'] = outputs['RasterCalculator']['OUTPUT']

        """Passage à la fonction suivante"""
        feedback.setCurrentStep(2)
        if feedback.isCanceled():
            return {}

        """Fonction : ZonalStatistics
                Entrées : 'outputs['RasterCalculator']['OUTPUT']' à savoir 'NdviDeLaZoneDtude' et 
                'outputs['ReprojeterUneCouche']['OUTPUT']' à savoir 'RpgReprojet'
                Sortie : 'ZonalStatNdvi' 
        Fonction du plugin Orfeo Tool Box (OTB) qui permet d'obtenir des métriques (moyenne, min, max, écart-type) des valeurs
        des pixels de l'image NDVI par parcelle du RPG."""
        
        alg_params = {
            'in': outputs['RasterCalculator']['OUTPUT'],
            'inbv': 0,
            'inzone': 'vector',
            'inzone.labelimage.in': outputs['RasterCalculator']['OUTPUT'],
            'inzone.labelimage.nodata': 0,
            'inzone.vector.in': outputs['ReprojeterUneCouche']['OUTPUT'],
            'inzone.vector.reproject': False,
            'out': 'vector',
            'out.raster.bv': 0,
            'out.xml.filename': 'XML',
            'outputpixeltype': 5,  # float
            'out.raster.filename': QgsProcessing.TEMPORARY_OUTPUT,
            'out.vector.filename': parameters['ZonalStatNdvi']
        }
        outputs['Zonalstatistics'] = processing.run('otb:ZonalStatistics', alg_params, context=context, feedback=feedback, is_child_algorithm=True)
        results['ZonalStatNdvi'] = outputs['Zonalstatistics']['out.vector.filename']

        """Passage à la fonction suivante"""
        feedback.setCurrentStep(3)
        if feedback.isCanceled():
            return {}

        """Fonction : Calculatrice de champs
                Entrées : 'ZonalStatNdvi'
                Sortie : 'ZonalStatNdvi' avec une colonne 'Couvert_hivernal'
        Cette fonction permet la création d'une colonne 'Couvert_hivernal' qui renvoie 1 ou 0 si la parcelle en question 
        remplit la condition suivante : '"mean_0" >= 0.40'. ('mean_0' est la moyenne de la bande 0 de l'image NDVI sur la 
        parcelle. Cette colonne est créée par le biais de la fonction ZonalStatistics). Cette condition permet de discriminer
        les parcelles ayant une valeur de NDVI inférieur et supérieur a 0.40. Ce seuil a été choisi en effectuant différent 
        test par le biais de composition colorée ainsi que la prise en compte de l'intensité végétale sur les mois de 
        décembre et janvier. Ainsi, le seuil de 0,40 permet de définir les parcelles ayant un couvert hivernal de celles 
        qui n'en possèdent pas """

        alg_params = {
            'FIELD_LENGTH': 0,
            'FIELD_NAME': 'Couvert_hivernal',
            'FIELD_PRECISION': 0,
            'FIELD_TYPE': 1,  # Entier (32bit)
            'FORMULA': '"mean_0" >= 0.40',
            'INPUT': outputs['Zonalstatistics']['out.vector.filename'],
            'OUTPUT': QgsProcessing.TEMPORARY_OUTPUT
        }
        outputs['CalculatriceDeChamp'] = processing.run('native:fieldcalculator', alg_params, context=context, feedback=feedback, is_child_algorithm=True)

        """Passage à la fonction suivante"""
        feedback.setCurrentStep(4)
        if feedback.isCanceled():
            return {}

        
        """Fonction : Supprimer les champs
                Entrées : 'ZonalStatNdvi' avec une colonne 'Couvert_hivernal'
                Sortie : 'RpgEnrichi'
        Nettoyage de la table attributaire des colonnes résiduels non utilisés de la fonction ZonalStatistics à savoir :
        les colonnes 'count','stdev_0','min_0','max_0'
        """

        alg_params = {
            'COLUMN': ['count','stdev_0','min_0','max_0'],
            'INPUT': outputs['CalculatriceDeChamp']['OUTPUT'],
            'OUTPUT': parameters['RpgEnrichi']
        }
        outputs['SupprimerChamps'] = processing.run('native:deletecolumn', alg_params, context=context, feedback=feedback, is_child_algorithm=True)
        results['RpgEnrichi'] = outputs['SupprimerChamps']['OUTPUT']
        return results


    "Paramètres définissant les métadonnées propre au format .model3"
    def name(self):
        return 'Modèle'

    def displayName(self):
        return 'Modèle'

    def group(self):
        return ''

    def groupId(self):
        return ''

    "Lancement de l'algorithme "
    def createInstance(self):
        return Modle()
