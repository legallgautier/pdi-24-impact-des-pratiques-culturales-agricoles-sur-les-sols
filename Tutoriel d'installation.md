## Read Me - Tutoriel d'installation

### <u>1- Installation d'Orfeo Tool Box + de son plugin QGIS</u> 

Une partie de notre algorithme de traitement d'image fonctionne grâce à un logiciel de télédetection nommé Orfeo Tool Box ou OTB. Il convient donc de l'installer en amont de l'utilisation de notre outil.

#### Téléchargement et installation d'OTB

Vous pouvez trouver le lien d’installation d'Orfeo ToolBox [ici](https://www.orfeo-toolbox.org/)

Une fois le zip téléchargé, extrayez les fichiers au sein d'un dossier proche de la racine du disque sur 	lequel vous souhaitez installer OTB. Cela aura son importance plus tard, lors de l'installation du plugin QGIS. Il n'y a pas d'installer, le produit OTB est directement utilisable une fois qu'il est dézippé.

*Exemple : C:\OTB-8.1.2-Win64*


#### Installation du plugin QGIS

Une fois le logiciel sur votre ordinateur, ouvrez QGIS. Allez dans *Extensions>
Installer/gérer les Extensions>Non Installés* et tapez dans la barre de recherche *Orfeo Toolbox provider* : 

Une fois installé, vérifiez que le plugin est activé en cochant la check box : 

![image](Screenshot_plugin_1.png)





![image](Screenshot_plugin_2.png)


![image](Screenshot_3_plugin.png)

Pour la prochaine étape, nous aurons besoin de la *Boîte à outils de traitement*. Normalement, elle se trouve par défaut sur la droite de votre fenêtre QGIS. Si celle-ci n'est pas ouverte, il vous suffit de l'ouvrir de cette manière : *Vue>Panneaux>Boîte à outils de traitement*

![image](Screenshot_4_plugin.png)

Une fois la boîte sous vos yeux, cliquez sur la clé à molette. Allez dans *Fournisseurs de services>OTB*

![image](Screenshot_5.png)







![image](Screenshot_6.png)

Comme sur les captures d'écran, il faut référencer le chemin où votre OTB est installé pour les options 	*Répertoire OTB* et *Répertoire des applications OTB*. 

**ATTENTION**, l'installation à la racine de votre disque est important pour cette étape là. Le plugin OTB semble avoir des problèmes de droit sous Windows pour l'ouverture de certain dossier comme *Programmes* ou *Programmes(x86)* par exemple. Il s'agit à la base d'un logiciel conçu pour être utilisé en ligne de commande et sous Linux principalement. L'installation à la racine permet de limiter ce problème.

![image](Screenshot_7.png)



Si le plugin d'OTB est correctement installé, vous devriez voir apparaître dans votre boîte d'outils un 	menu déroulant OTB

![image](Screenshot_8.png)

#### Intégrer un modeleur directement sur QGIS.

Pour cela, il suffit d'utiliser le bouton *modèles* (les engrenages rouge, bleu et gris au niveau de la *Boite à outils de traitement*) :

![image](Screenshot_9.png)

### <u>2- Installation du plugin GeoField via un .zip :</u>

Pour installer le plugin GeoField via son .zip, il suffit de retourner dans *Extensions>Installer/gérer les 	Extensions>Installer depuis un .zip*. Il suffit ensuite de sélectionner le .zip du plugin sur votre ordinateur 	et activer si  

![image](Screenshot_plugin_1.png)


![image](Screenshot_10.png)

### <u>3- Mise en place préliminaire à l’utilisation des APIs</u> 

Nos algorithmes de récupération de données exploite deux APIs. Une seule d’entre elles nécessite des étapes de mise en place

#### Création de l’ID sur Sentinel Hub

L’outil utilise plusieurs APIs dont SentinelHub qui est en lien avec une plateforme associée, connectée 	à un cloud. 

Afin d’éviter d’utiliser en aucun cas des librairies externes à ceux déjà fournies par le logiciel QGIS, le 	choix s’est porté sur l’utilisation de « Request Builder », qui permet d’accéder plus simplement à l’API 	de Sentinel Hub pour l’obtention des données. 

​	**Rappel** : Il existe la possibilité d’utiliser aussi Odata qui permet d’accéder gratuitement aux données             	sous forme de [catalogue](https://documentation.dataspace.copernicus.eu/APIs/OData.html) ([*https://documentation.dataspace.copernicus.eu/APIs/OData.html*](https://documentation.dataspace.copernicus.eu/APIs/OData.html))



![](C:\Users\ericl\Desktop\plugin_geofield\scree_leo1.png)





Pour des utilisateurs avisés, cet API pourrait être plus intéressante notamment par sa gratuité. Néanmoins, il est nécessaire d’insérer les identifiants de connexion supposant soit le choix que l’utilisateur doit pouvoir se créer son propre compte et remplir les différents critères de connexion sur QGIS en se connectant sur l’API, soit fournir un  accès par un compte général (créé par le gestionnaire du plugin et configurer les paramètres directement sur python). 

Dans le cadre d’une meilleure opérabilité pour le gestionnaire malgré le coût associé pour l’achat d’une licence « commerciale », nous parlerons de la création de cet ID qui permet l’obtention des images satellites. 



**Etape 1 : Connexion à la plateforme**

- Se connecter sur Sentinel Hub sur le tableau de bord : [*https://apps.sentinel-hub.com/dashboard*](https://apps.sentinel-hub.com/dashboard)*.*
- Aller dans l’onglet « Configuration Utility » puis créer une nouvelle configuration.

![](C:\Users\ericl\Desktop\plugin_geofield\screen_leo2.png)



![](C:\Users\ericl\Desktop\plugin_geofield\screen_leo3.png)


**Etape 2 : Paramétrage**

Il faut : 

- Retirer le logo et améliorer la qualité de l’image
- Créer une nouvelle couche (avec la source de données donc Sentinel-2 puis la période de temps. Il est important de mettre la couverture nuageuse la plus faible possible. **ATTENTION :**Parfois la couverture nuageuse peut être à 0% mais l’image peut être de très mauvaise qualité avec de très nombreux nuageuses (0% intègre selon Copernicus, les images satellite dont ils ont aucune information)
- Il faut aussi configurer le triage « Mosaic Order » des données satellites qui vont sortir de cette recherche (dans le catalogue).  
- Il est important de pouvoir choisir dans « Data Processing », ses données avec une possibilité de modifier au besoin le script de requête.  

![image-20240513205533561](C:\Users\ericl\AppData\Roaming\Typora\typora-user-images\image-20240513205533561.png)

**Etape 3 : Finalisation :** 

Désormais il est possible de récupérer l’ID de votre requête. Il sera à insérer dans l’URL de la requête dans le script Python.

**Etape 4 :** Accès à « Request Builder » que vous pouvez faire en cliquant sur ce lien : [*https://apps.sentinel-hub.com/requests-builder/*](https://apps.sentinel-hub.com/requests-builder/) . 

**Etape 5 :** Choix de l’API 

(La connexion est déjà établie : il n’y a pas besoin de se reconnecter) Dans la sélection d’Api, le choix a été fait de prendre « OGC Services » pour mettre en place un lien de connexion qui permet un téléchargement direct de l’image.

**Etape 7 : Paramétrage de « Request Builder ».**

Le choix se fait pour un lien WMS. Dans les options d’instance, il suffit de récupérer la configuration choisie dans l’étape 2-3, de spécifier la période temporelle de recherche, et la projection. Il est possible de modifier l’aspect, le format ect… Cependant, toutes les variables peuvent être modifiable en intégrant ces variables sur python dans l’URL qui est fourni après paramétrage. (Ne pas oublier d’insérer l’ID).

![image-20240513205722069](C:\Users\ericl\AppData\Roaming\Typora\typora-user-images\image-20240513205722069.png)

Nous utilisons « requests.get » sur le script python pour récupérer l’image. Pour plus d’informations : [*https://docs.sentinel-hub.com/api/latest/user-guides/beginners-guide/#requests-builder*](https://docs.sentinel-hub.com/api/latest/user-guides/beginners-guide/#requests-builder) (Lien de la documentation).



### <u>4-Téléchargement et installation de PostgreSQL et PostGIS</u>



Pour ce tutoriel nous utilisons pgAdmin.Il s’agit d’un logiciel gratuit sous licence PostgreSQL, si vous le souhaitez, d’autres interfaces graphiques sont possible pour PostgreSQL. Téléchargez la version adaptée à votre système depuis ce lien : https://www.pgadmin.org/download/

Pour les systèmes Windows, il est recommandé d'utiliser l'installateur interactif par EDB. Cet installateur est conçu pour être une méthode simple et rapide pour démarrer avec PostgreSQL sur Windows : https://www.enterprisedb.com/downloads/postgres-postgresql-downloads

Après le téléchargement, suivez le tutoriel détaillé pour installer PostgreSQL : https://www.enterprisedb.com/docs/supported-open-source/postgresql/installing/windows/



**Attention :** Avant que l'assistant d'installation ne termine l'installation de PostgreSQL, il propose de lancer Stack Builder à la sortie. Laissez la case cochée et sélectionnez "Finish" pour lancer Stack Builder. Cet utilitaire fournit une interface graphique pour télécharger et installer des applications et pilotes compatibles avec PostgreSQL.



Grâce à son extensibilité, PostgreSQL offre une grande variété de types de données intégrés, y compris JSON, XML, HSTORE (clé-valeur), Geo-spatial (PostGIS), IPv6. Nous utiliserons PostGIS par la suite. Suivez les instructions de Stack Builder et cochez la case PostGIS Bundle 3 pour PostgreSQL. PostGIS sera installé dans le PostgreSQL correspondant.



![bdd2](C:\Users\ericl\Desktop\installation de PostgreSQL et PostGIS\bdd2.png)



Si vous avez accidentellement fermé Stack Builder, vous pouvez généralement le trouver dans le chemin suivant : Start > Programs > PostgreSQL > Application Stackbuilder



Voici la documentation détaillée pour installer PostGIS, où vous trouverez presque toutes les réponses à vos questions : https://www.bostongis.com/PrinterFriendly.aspx?content_name=postgis_tut01



#### Création d'une base de données



Après avoir ouvert pgAdmin, cliquez sur "servers" et entrez le mot de passe défini lors de l'installation pour activer la connexion à PostgreSQL. Dans le sous-répertoire, cliquez droit sur "Databases" et créez une nouvelle base de données. Si vous avez encore des questions, consultez la documentation suivante : https://docs.postgresql.fr/16/tutorial-createdb.html



![bdd1](C:\Users\ericl\Desktop\installation de PostgreSQL et PostGIS\bdd1.png)



#### Activation de PostGIS

Pour activer PostGIS dans la base de données, exécutez dans l'outil de requête :



```sql
CREATE EXTENSION postgis;
```



![bdd3](C:\Users\ericl\Desktop\installation de PostgreSQL et PostGIS\bdd3.png)



Vous pouvez vérifier la version que vous avez avec :

```sql
SELECT PostGIS_Full_Version();
```

#### Connexion de la base de donnée QGIS

Dans QGIS, ouvrir le panneau « Data source manager » (l'outil pour ajouter de nouvelles sources de donnée). Puis sélectionner l’onglet Postgres SQL. Cliquer sur « New ». Puis, configurer la connexion à la base de donnée, en lui donnant un nom et en renseignant l’adresse dans « host ». Pour connaître cette adresse, on peut aller dans les propriétés de la base de donnée dans Pgadmin. Il faut penser à configurer le port, il doit être disponible. On peut après tester la connexion et la base de données est enfin configurée.



#### Importer les schémas de la base de donnée

Dans QGIS, ouvrir l'outil « BD manager », sélectionner la fonctionnalité POSTgis. Sélectionner sa base de données. Puis se connecter : si on n’a pas configuré d’utilisateur pour la BDD, l’utilisateur et le mot de passe par défaut sont « postgres ». Une fois que l’on est connecté, on peut aller dans « import layer » et configurer le schéma à utiliser, généralement, le schéma « public ».

Pour cette partie, référez-vous à la vidéo de démonstration nommée “BDD_vidéo.mp4”

