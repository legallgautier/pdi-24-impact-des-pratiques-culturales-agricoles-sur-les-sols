











### <u>1-Tutoriel Utilisation Couvert Hivernal</u>

Etape 1 : Ouvrir le modeleur “Couvert_hivernal.model3”

Pour cela il suffit d’aller dans : Traitement>Modeleur Graphique>Ouvrir le modèle.

#### Etape 1 : Rentrer les chemins données en entrée 

Il y a trois données en entrée :

- La bande Rouge (B4) de l’image Sentinel 2, il s’agit d’un .tif
- La bande Proche Infrarouge de l’image Sentinel 2,  il s’agit d’un .tif
- Le RPG amélioré, qui est la sortie du premier algorithme d’enrichissement. Il faut rentrer le .shp de l’année souhaité

Il faut aussi rentrer le SCR souhaité, il est fortement conseillé d’utiliser l’UTM zone adapté à la zone d’étude.

#### Etape 2 : Définir les chemins des sorties des fonctions intermédiaires et le produit final de l’algorithme

L’utilisation d’Orfeo Tool Box contraint l’utilisateur a devoir enregistrer une partie des résultats des fonctions intermédiaires  de l’algorithme. Ainsi, il faut enregistrer : 

- Le RPG reprojeté, sortie de la fonction “Reprojection”
- Le NDVI construit par la fonction “Calculatrice Raster”
- Le produit final de la fonction “ZonalStatistics” à savoir “Zonal Stat NDVI”

Enfin, il faut enregistrer la sortie de l’algorithme, à savoir “RPG_Enrichi”

Les paramètres non cités peuvent être laissés en couche temporaire

Vous pouvez aussi vous appuyer sur la vidéo de démonstration “Demo_Algo_couvert_Hivernal.mp4”

Vous pouvez dorénavant lancer le modeleur.

### <u>2-Tutoriel Utilisation Algorithme Enrichissement des données</u>

#### Etape 1 : Ouvrir le modeleur “RPG_Enrichi_modeleur.model3”

Pour cela il suffit d’aller dans : Traitement>Modeleur Graphique>Ouvrir le modèle.

#### Etape 2 : Définir les chemins données en entrée et le produit final de l’algorithme

Il y a deux données en entrée :

- 1) Le RPG de la n-ième année, il s’agit d’une couche .shp.
- 2) Le RPG de la n+1-ième année, il s’agit d’une couche .shp 

Il y a une donnée en sortie : 

- Le RPG amélioré, qui est la sortie de l’algorithme d’enrichissement, entre l’année n et n+1. 

On obtient alors un résultat qui témoigne des évolutions entre l’année n et n+1. On peut répéter cette opération autant de fois que souhaiter pour obtenir un RPG qui contient les informations d’évolutions  sur une période donnée. Ainsi, si je souhaite couvrir une année n+2 au sein de mon RPG enrichi: Je relance le modeleur en entrant dans les paramètres cette fois-ci:

Il y a deux données en entrée :

- 1. Je relance le modeleur en entrant dans les paramètres cette fois-ci le RPG amélioré entre l’année n et n+1 
- 2. Je sélectionne le RPG pour l’année n+2

Il y a une donnée en entrée :

- Le RPG amélioré, qui est la sortie de l’algorithme d’enrichissement, entre l’année n et n+2. 

### <u>3- Tutoriel Utilisation du plugin de chargement des données – Geofield</u>



Un plugin expérimental a été réalisé pour mettre en avant le résultat du projet. Le script évite toute utilisation de librairie python annexe nécessitant une installation supplémentaire de la part de l’utilisateur. 

Actuellement le plugin n’est pas ouvert à la communauté, ni téléchargeable par un quelconque catalogue QGIS ou sur internet. 

Les données nécessaires pour le fonctionnement des autres étapes de Geofield que va récupérer le plugin : 

- Données du Registre Parcellaire Graphique de 2015 à 2022 (Données Vecteurs |Shapefile) de l’IGN
- Données d’Imagerie Satellite de Sentinel 2 issu du programme européen « Copernicus » de 2015 à 2022 (Données Rasters | TIFF)
- Un fond cartographique avec l’utilisation d’un flux WMS provenant aussi de l’IGN (Institut national de l’information géographique et forestière)

L’utilisation du plugin nécessite que peu d’étapes.

#### 1ère étape – L’installation :

Son installation peut se faire de deux manières pour pouvoir l’utiliser sur QGIS :

- Soit en déposant les documents « geofield » dans votre dossier contenant vos extensions de QGIS. Généralement, le chemin d’accès est : « ...\AppData\Roaming\QGIS\QGIS3\profiles\default\python\plugins »

![img](file:///C:/Users/ericl/AppData/Local/Temp/lu18836ir96c.tmp/lu18836ir96m_tmp_98a536bedda67866.png) 

*Fenêtre des extensions sur QGIS* 

- Soit en utilisant le gestionnaire d’extensions de QGIS et en installant depuis un .zip (en compressant le document geofield). Ce dernier sera directement déposé dans votre dossier d’extensions QGIS et pourra être utilisable.

L’outil peut se lancer directement sur le projet en cliquant dans l’onglet « Extensions » de QGIS où vous retrouverez Geofield.

#### 2ème étape – l’utilisation :

Le plugin est simple puisqu’il prend en entrée les années que l’utilisateur souhaite analyser et peut ajouter une couche de son projet comme zone d’étude pour délimiter la zone dont il souhaite acquérir les données. 

- ​	Insertion d’un chemin d’accès :

Il est important de bien organiser les couches qui seront téléchargées et donc de créer un dossier pour chaque requête si vous réalisez plusieurs requêtes d’une même année.

- ​	Gestion des années :

Pour cette partie, il faut simplement inscrire les années séparées de virgules. 

**Attention : actuellement les données RPG sont limitées (de 2015 à 2021). Les prochaines années dépendront des ajouts de la part de l’IGN. De plus, il se peut arriver qu’aucune donnée Sentinel soit disponible (période paramétrée entre le 1er décembre et le 31 janvier) si la qualité de l’image n’est pas suffisante. Le niveau de zoom peut avoir une influence sur la résolution de l’image.**

- ​	Gestion de l’emprise de l’étude :

Dans cette version, l’outil peut être utilisé de deux manières :

Soit il faut intégrer une couche « vecteur » (qui doit être obligatoirement un **polygone simple** (couche avec 1 entité) où cela va générer une erreur lors du fonctionnement de l’outil) comme zone d’étude. 

Soit utiliser l’outil par défaut, c’est-à-dire en prenant en compte les coordonnées de l’écran du projet QGIS en cours.

![img](file:///C:/Users/ericl/AppData/Local/Temp/lu18836ir96c.tmp/lu18836ir96m_tmp_6947811a2a0056b0.png)

La gestion des projections est automatique. Que vous soyez en Lambert 93 ou autre, vous serez à la fin en UTM Zone. 

3 projections sont ainsi utilisées :

- ​	EPSG :32630 | WGS 84 UTM Zone 30 Nord
- ​	EPSG :32631 | WGS 84 UTM Zone 31 Nord
- ​	EPSG :32632 | WGS 84 UTM Zone 32 Nord













