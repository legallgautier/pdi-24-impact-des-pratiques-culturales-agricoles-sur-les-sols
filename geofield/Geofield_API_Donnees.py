import sys
from qgis.gui import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import QPixmap
from PyQt5.QtCore import *
from qgis.core import *
from qgis.utils import iface
import requests, json, os

class DataDownloadTool(QMainWindow):
    def __init__(self):
        """
        Via les outils de QtWidgets / QTGUI / QTcore : Configuration de l'interface utilisateur pour le plugin
        """
        super().__init__()

        #titre et dimensions de la fenêtre
        self.setWindowTitle("Geofield - QGIS au service de l'Agriculture")
        self.setGeometry(400, 400, 1200, 600)

        #Paramétrage générale de la gestion de style 
        main_widget = QWidget()
        self.setCentralWidget(main_widget)
        main_layout = QVBoxLayout()
        main_widget.setLayout(main_layout)

        #Mise en place de l'image
        script_dir = os.path.dirname(__file__) #chemin du script en cours
        geofield_icon = os.path.join(script_dir, "Logo_Geofield.png")
        pixmap = QPixmap(geofield_icon)
        scaled_pixmap = pixmap.scaled(600, 400, Qt.KeepAspectRatio, Qt.SmoothTransformation) #Gestion taille et style
        image_label = QLabel()
        image_label.setAlignment(Qt.AlignCenter)
        image_label.setPixmap(scaled_pixmap)
        main_layout.addWidget(image_label)

        # Information pour l'utilisateur sur le chemin d'accès
        info_label = QLabel("Choisissez un chemin d'accès pour stocker les données RPG et SentinelHub.")
        main_layout.addWidget(info_label)

        # Widget pour le chemin d'accès
        path_widget = QWidget()
        path_layout = QVBoxLayout()
        path_widget.setLayout(path_layout)

        path_label = QLabel("Chemin d'accès :")
        self.line_edit_path = QLineEdit()
        self.line_edit_path.setReadOnly(True)
        self.button_choose_path = QPushButton('Choisir le chemin')
        self.button_choose_path.clicked.connect(self.choosePath)

        path_layout.addWidget(path_label)
        path_layout.addWidget(self.line_edit_path)
        path_layout.addWidget(self.button_choose_path)

        main_layout.addWidget(path_widget)

        # Widget pour saisir les années
        years_widget = QWidget()
        years_layout = QVBoxLayout()
        years_widget.setLayout(years_layout)

        years_label = QLabel('Année(s) (séparées par des virgules) :')
        self.line_edit_years = QLineEdit()

        years_layout.addWidget(years_label)
        years_layout.addWidget(self.line_edit_years)

        main_layout.addWidget(years_widget)

        # Widget pour sélectionner la couche avec un seul polygone
        layer_selection_widget = QWidget()
        layer_selection_layout = QVBoxLayout()
        layer_selection_widget.setLayout(layer_selection_layout)

        layer_label = QLabel('Sélectionnez une couche contenant un seul polygone (Pas Multipolygon):')

        layer_selection_layout.addWidget(layer_label)
        self.layer_combo_box = QgsMapLayerComboBox()
        self.layer_combo_box.setFilters(QgsMapLayerProxyModel.VectorLayer)
        self.layer_combo_box.addItem(" ") #Valeur vide par défaut 
        layer_selection_layout.addWidget(self.layer_combo_box)
        main_layout.addWidget(layer_selection_widget)

        self.checkbox_enable_layer_selection = QCheckBox('Activer la sélection de couche')
        self.checkbox_enable_layer_selection.setChecked(True)
        self.checkbox_enable_layer_selection.stateChanged.connect(self.toggleLayerSelection)
        main_layout.addWidget(self.checkbox_enable_layer_selection)

        self.setLayout(main_layout)
        self.show()

        # Zone de texte pour afficher les messages
        self.text_edit_console = QPlainTextEdit()
        self.text_edit_console.setReadOnly(True)
        main_layout.addWidget(self.text_edit_console)

        # Bouton pour démarrer le téléchargement-traitement des données 
        self.button_start_download = QPushButton('Démarrer le téléchargement')
        self.button_start_download.clicked.connect(self.confDownload) #Connexion à confDownload > qui va paramétrer en fonction des informations d'entrée puis lancer "execute_script"
        main_layout.addWidget(self.button_start_download)


    def toggleLayerSelection(self, state):
        """
        Active ou désactive la sélection de la couche dans la boîte de combinaison en fonction de l'état de la case à cocher.
        """
        if state == Qt.Checked:
            self.layer_combo_box.setEnabled(True)
        else:
            self.layer_combo_box.setEnabled(False)

    def choosePath(self):
        """
        Affiche une boîte de dialogue pour permettre à l'utilisateur de choisir un dossier de destination
        pour enregistrer les données téléchargées.
        """
        options = QFileDialog.Options()
        file_path = QFileDialog.getExistingDirectory(self, 'Choisir le dossier de destination', options=options)
        if file_path:
            self.line_edit_path.setText(file_path)

    def confDownload(self):
        """
        Préparation pour le téléchargement des données | Informations sur la console à propos de la configuration choisie
        """
        #Récupération des informations laissées en entrée par l'utilisateur
        file_path = self.line_edit_path.text()
        input_years = self.line_edit_years.text()
        selected_layer_name = self.layer_combo_box.currentText()
        if selected_layer_name:
            selected_layers = QgsProject.instance().mapLayersByName(selected_layer_name)
            selected_layer = selected_layers[0]

        if not file_path:
            self.appendConsoleMessage("Veuillez choisir un chemin d'accès valide.")
            return

        if not input_years:
            self.appendConsoleMessage("Veuillez saisir au moins une année.")
            return

        # Inscription des années choisies sur la console de dialogue 
        selected_years = [int(year.strip()) for year in input_years.split(",") if year.strip()]
        
        if not selected_years:
            self.appendConsoleMessage("Veuillez saisir au moins une année valide.")
            return

        # Informations sur les paramètres mis en entrée 
        self.appendConsoleMessage(f"Chemin d'accès choisi : {file_path}")
        self.appendConsoleMessage(f"Années sélectionnées : {selected_years}")
        if selected_layer_name:
            self.appendConsoleMessage(f"Couche sélectionnée : {selected_layer}")
            self.execute_script(file_path, input_years, selected_layer)
        else:
            self.appendConsoleMessage("Mode par défaut actif (Prise des coordonnées du canvas)")
            self.execute_script(file_path, input_years, None) 

        self.appendConsoleMessage("Traitement terminé avec succès.")


    def execute_script(self,file_path,selected_years,selected_layer):
        """
        Lancement du script général de téléchargement / manipulation des données qui sont ensuite ajoutées sur le projet QGIS
        """
        # Récupération des coordonnées de l'écran de l'utilisateur 
        extent = iface.mapCanvas().extent()

        actual_proj=iface.mapCanvas().mapSettings().destinationCrs() #Obtention de la projection du projet de l'utilisateur
        transform_to_wgs84 = QgsCoordinateTransform(actual_proj, QgsCoordinateReferenceSystem('EPSG:4326'), QgsProject.instance())
        
        #-------------------------------------------------------
        #Mise en place du fond (Plan IGN) par flux WMS

        #Si le PLAN IGN existe déjà
        if not QgsProject.instance().mapLayersByName('PLAN_IGN'):
            wms_url = (f"crs=EPSG:4326&dpiMode=7&format=image/png&layers=GEOGRAPHICALGRIDSYSTEMS.PLANIGNV2&styles&url=https://data.geopf.fr/wms-r?VERSION%3D1.3.0") #Mise en place des paramètres et de l'url du WMS
            wms_layer = QgsRasterLayer(wms_url, 'PLAN_IGN', 'wms') 
            if not wms_layer.isValid():
                self.appendConsoleMessage("Erreur avec la couche de fond topographique") 
            else:
                self.appendConsoleMessage("Flux WMS chargé")
                QgsProject.instance().addMapLayer(wms_layer)
        #------------------------------------------------------------------------------------------------
        #Dans le cas où l'utilisateur insère une couche contenant un polygone délimitant la zone d'étude
        layer_import=selected_layer
        if self.checkbox_enable_layer_selection.isChecked() and selected_layer: #Si le paramètre par défaut est désactivé
            if layer_import.featureCount() == 1: #Condition : Il faut qu'une entité
                actual_proj = layer_import.crs()
                features = layer_import.getFeatures()
                poly_geom_pt = []
                poly_geom_ = []
                for feature in features: #Récupération pour chaque entité des informations sur la géométrie et les coordonnées 
                    geoms = feature.geometry()
                    bbox = geoms.boundingBox() #Mesure du plus petit rectangle englobant (sMBR) les géométries (pour les données SentinelHub)
                    coords = geoms.asPolygon()
                    for points in coords:
                        for point in points:
                            x,y = (point.x()),(point.y())
                        if actual_proj.authid() != "EPSG:3857" and actual_proj.authid() != "EPSG:4326": #Transformation en WGS84 nécessaire pour l'API Carto (RPG)
                            transformed_point = transform_to_wgs84.transform(QgsPointXY(point[0], point[1]))
                            poly_geom_pt.append(QgsPointXY(transformed_point.x(), transformed_point.y()))
                            poly_geom_.append([transformed_point[0], transformed_point[1]])
                        else:
                            poly_geom_pt.append(QgsPointXY(point[0], point[1]))
                            poly_geom_.append([point[0], point[1]])

                        #Recherche de la valeur minimum du point pour évaluer la position du canva et effectuer un choix sur les projections UTM
                        min_pt=min(poly_geom_pt, key=lambda poly_pt:poly_pt.x()) 
                        min_geom = min_pt.x()
                        if -6 <= min_geom < 0: 
                            new_proj = '32630'
                        elif 0 <= min_geom <= 6:
                            new_proj = '32631'
                        elif 6 < min_geom <= 12:
                            new_proj = '32632'
                        else:
                            self.appendConsoleMessage("Erreur : Vous êtes en dehors de la France Métropolitaine")
                        transform_proj = QgsCoordinateTransform(actual_proj, QgsCoordinateReferenceSystem(f'EPSG:{new_proj}'), QgsProject.instance())
                        bbox_new_proj = transform_proj.transform(bbox) #Pour SentinelHub
                        lower_left_str = f'{bbox_new_proj.xMinimum()},{bbox_new_proj.yMinimum()}'
                        upper_right_str = f'{bbox_new_proj.xMaximum()},{bbox_new_proj.yMaximum()}'
            else:
                self.appendConsoleMessage("Erreur : L'outil ne prend en charge que des couches avec 1 seul polygone")

            #Fin du paramétrage lorsque l'utilisateur met une couche en entrée
            #------------------------------------------------------------------------------------------------
            #Dans le cas où l'utilisateur ne met rien et coche la checkbox sur l'interface utilisateur
        else :
            #Transformation des points par les outils QGIS  et récupération des coordonnées de l'interface QGIS de l'utilisateur 
            lower_left_pt = QgsPointXY(float(extent.xMinimum()), float(extent.yMinimum()))
            upper_right_pt = QgsPointXY(float(extent.xMaximum()), float(extent.yMaximum()))
            upper_left_pt= QgsPointXY(float(extent.xMinimum()),float(extent.yMaximum()))
            lower_right_pt= QgsPointXY(float(extent.xMaximum()),float(extent.yMinimum()))

            #Transformation des coordonnées en WGS84 pour les APIs
            if actual_proj is not "QgsCoordinateReferenceSystem: EPSG:3857" or "QgsCoordinateReferenceSystem: EPSG:4326" :
                lower_left_wgs84 = transform_to_wgs84.transform(lower_left_pt)
                upper_right_wgs84 = transform_to_wgs84.transform(upper_right_pt)
                upper_left_wgs84 = transform_to_wgs84.transform(upper_left_pt)
                lower_right_wgs84 = transform_to_wgs84.transform(lower_right_pt)

            #En fonction de la région en France : Adaptation de la projection UTM 
                if -6 <= upper_left_wgs84.x() < 0: #Conditions observées à partir d'une projection WGS84 (latitude/longitude)
                    new_proj = '32630'
                elif 0 <= upper_left_wgs84.x() <= 6:
                    new_proj = '32631'
                elif 6 < upper_left_wgs84.x() <= 12:
                    new_proj = '32632'
                else:
                    self.appendConsoleMessage("Erreur : Vous êtes en dehors de la France Métropolitaine")
                
                transform_proj = QgsCoordinateTransform(actual_proj, QgsCoordinateReferenceSystem(f'EPSG:{new_proj}'), QgsProject.instance())
                lower_left_new_proj = transform_proj.transform(lower_left_pt)
                lower_left_str = f'{lower_left_new_proj.x()},{lower_left_new_proj.y()}'
                upper_right_new_proj = transform_proj.transform(upper_right_pt)
                upper_right_str = f'{upper_right_new_proj.x()},{upper_right_new_proj.y()}'

        #De la récupération des données d'entrée de l'utilisateur sur les années -> Mise en place des valeurs pour les requêtes vers les APIs
        annee=[]
        results_year = selected_years.split(",")  #Segmentation des valeurs
        for i in range(len(results_year)):
            results_year[i] = results_year[i].strip() #Retrait des éventuels espaces mis par l'utilisateur sur la valeur
            annee.append(results_year[i]) 

        #------------------------------------------------------------------------------------------------
        #Partie 1 : Api pour Sentinel Hub
        
        #Mise en place de la requete
        for i in annee:
            if layer_import and self.checkbox_enable_layer_selection.isChecked():
                wms_url_B8 =f'https://services.sentinel-hub.com/ogc/wms/6a50cb2d-3889-45e2-b213-2223f6449103?REQUEST=GetMap&CRS=EPSG:{str(new_proj)}&BBOX={str(lower_left_str)},{str(upper_right_str)}&LAYERS=SENTINEL_2_B8&WIDTH=512&HEIGHT=343&FORMAT=image/tiff&TIME={str(i)}-12-01/{str(int(i) + 1)}-01-31'
                wms_url_B4 = f'https://services.sentinel-hub.com/ogc/wms/6a50cb2d-3889-45e2-b213-2223f6449103?REQUEST=GetMap&CRS=EPSG:{str(new_proj)}&BBOX={str(lower_left_str)},{str(upper_right_str)}&LAYERS=SENTINEL_2_B4&WIDTH=512&HEIGHT=343&FORMAT=image/tiff&TIME={str(i)}-12-01/{str(int(i) + 1)}-01-31' 
            else:
                wms_url_B8 = f'https://services.sentinel-hub.com/ogc/wms/6a50cb2d-3889-45e2-b213-2223f6449103?REQUEST=GetMap&CRS=EPSG:{str(new_proj)}&BBOX={lower_left_str},{upper_right_str}&LAYERS=SENTINEL_2_B8&WIDTH=512&HEIGHT=343&FORMAT=image/tiff&TIME={str(i)}-12-01/{str(int(i) + 1)}-01-31'
                wms_url_B4 = f'https://services.sentinel-hub.com/ogc/wms/6a50cb2d-3889-45e2-b213-2223f6449103?REQUEST=GetMap&CRS=EPSG:{str(new_proj)}&BBOX={str(lower_left_str)},{str(upper_right_str)}&LAYERS=SENTINEL_2_B4&WIDTH=512&HEIGHT=343&FORMAT=image/tiff&TIME={str(i)}-12-01/{str(int(i) + 1)}-01-31' 

            path_B4 = f'{file_path}/Sentinel_1_B4_{i}.tif' 
            path_B8 = f'{file_path}/Sentinel_1_B8_{i}.tif'
            

            # Requete pour récupérer l'image
            requete_B8 = requests.get(wms_url_B8)
            requete_B4 = requests.get(wms_url_B4)
            
            with open(path_B4, 'wb') as f:
                f.write(requete_B4.content)
            with open(path_B8, 'wb') as f:
                f.write(requete_B8.content)

            self.appendConsoleMessage(f"Image téléchargée : {path_B4}")
            self.appendConsoleMessage(f"Image téléchargée : {path_B8}")

            # Charger la couche raster dans QGIS
            img_raster_B4 = QgsRasterLayer(path_B4, f'Bande 4 SentinelHub{i}/{str(int(i) + 1)}')
            img_raster_B8 = QgsRasterLayer(path_B8, f'Bande 8 SentinelHub{i}/{str(int(i) + 1)}')

            #Test pour savoir si l'image satellite ne possède aucune donnée (Sentinel n'arrive pas à trouver une donnée avec une couverture nuageuse inférieure à 5 %)
            extent = img_raster_B4.extent() # Récupération l'étendue spatiale de l'image raster Bande 4
            raster_provider=img_raster_B4.dataProvider() #Accès à la donnée raster 

            #Recherche si le raster contient de la donnée (si aucune image satellite ne répond aux critères envoyés par la requête, la donnée sera vide et donc à supprimer)
            stats = raster_provider.bandStatistics(1, QgsRasterBandStats.All, extent)
            max = stats.maximumValue
            if max == 0:
                self.appendConsoleMessage(f'Aucune donnée satellite disponible de qualité suffisante pour la saison hivernale de {i}')
                QgsProject.instance().removeMapLayer('Bande 4 SentinelHub')
                QgsProject.instance().removeMapLayer('Bande 8 SentinelHub') #Suppression des deux couches rasters (si l'une est défectueuse l'autre aussi)

            else :
                if img_raster_B4.isValid() :
                    # Ajout de la couche à la carte QGIS. Changement de la symbologie en bande grise 
                    renderer = QgsSingleBandGrayRenderer(img_raster_B4.dataProvider(), 1)  # 1 correspond au numéro de la bande (monobande)
                    img_raster_B4.setRenderer(renderer)
                    img_raster_B4.triggerRepaint()  #actualise l'affichage

                    renderer = QgsSingleBandGrayRenderer(img_raster_B8.dataProvider(), 1)  
                    img_raster_B8.setRenderer(renderer)
                    img_raster_B8.triggerRepaint()
                    QgsProject.instance().addMapLayer(img_raster_B4)
                    QgsProject.instance().addMapLayer(img_raster_B8)
                    self.appendConsoleMessage("Couches B4 et B8 ajoutées à QGIS")
                else:
                    self.appendConsoleMessage("Echec du chargement du raster B4 et B8.")

        #------------------------------------------------------------------------------------------------
            #Partie 2 : Connexion et chargement des données pour les données RPG de l'IGN

        for i in annee:
            self.appendConsoleMessage(f"Récupération des données pour l'année:{i}")

            #Préparation
            total_objets = 0 
            donnees = []
            fp = f'{file_path}/Donnees_RPG_{i}.geojson'
            file_path_shp = f'{file_path}/Donnees_RPG_{i}.shp'

            donneerestante=True
            #Création du polygone avec des données WGS84 (obligatoire)
            while donneerestante: #Pour la réalisation de plusieurs requêtes (Limite pour une seule requête : 1000 entités)
                if layer_import and self.checkbox_enable_layer_selection.isChecked():
                    geom_json = {"type":"MultiPolygon","coordinates":[[poly_geom_]]}
                else:
                    geom_json = {"type":"MultiPolygon","coordinates":[[[[lower_left_wgs84[0], lower_left_wgs84[1]],[upper_left_wgs84[0], upper_left_wgs84[1]],[upper_right_wgs84[0], upper_right_wgs84[1]],[lower_right_wgs84[0], lower_right_wgs84[1]],[lower_left_wgs84[0], lower_left_wgs84[1]]]]]}
                geom_str = json.dumps(geom_json)  # Conversion de la géométrie en chaîne JSON
                
                #Envoi de la requête (via une requête GET)
                url_api = 'https://apicarto.ign.fr/api/rpg/v2'
                parametres = {
                    'annee': i,
                    'geom': geom_str,
                    '_start': total_objets
                }
                response = requests.get(url_api, params=parametres)

                if response.status_code == 200:
                    objets = response.json().get('features', []) #Récupération des données en sortie JSON
                    donnees.extend(objets) #Récupération des données pour chaque requête
                    total_objets += len(objets)
                    

                if len(objets) == 0:
                    break 

            with open(fp, 'w') as f:
                json.dump({
                    "type": "FeatureCollection",
                    "features": donnees
                }, f)

            # Créer un objet QgsVectorLayer à partir du fichier GeoJSON
            geojson_data = QgsVectorLayer(fp, '', 'ogr')

        
            # Définir le système de coordonnées du Shapefile
            crs = QgsCoordinateReferenceSystem('EPSG:4326')  # WGS84
            geojson_data.setCrs(crs)

            # Convertir le fichier GeoJSON en Shapefile
            test = QgsVectorFileWriter.writeAsVectorFormat(geojson_data, file_path_shp, 'UTF-8', crs, 'ESRI Shapefile')

            if test == QgsVectorFileWriter.NoError:
                self.appendConsoleMessage(f"Le fichier Shapefile '{file_path_shp}' a été créé avec succès.")
            else:
                self.appendConsoleMessage(f"Une erreur s'est produite lors de la création du fichier Shapefile '{file_path_shp}'.")
            layer_response = QgsVectorLayer(file_path_shp, f'RPG_{i}', 'ogr')

            #Mise en place du CRS en fonction de la position de l'écran
            if new_proj == '32630':
                new_proj_=QgsCoordinateReferenceSystem(32630)
            elif new_proj == '32631':
                new_proj_=QgsCoordinateReferenceSystem(32631)
            elif new_proj == '32632' :
                new_proj_=QgsCoordinateReferenceSystem(32632)


            #Mise en place de la bonne projection (Car les données en sortie de l'API sont en WGS84)
            for feat in layer_response.getFeatures():
                geom = feat.geometry()
                old_proj=QgsCoordinateReferenceSystem(4326)
                transfproj=QgsCoordinateTransform(old_proj, new_proj_, QgsProject.instance())
                geom.transform(transfproj) 
                feat.setGeometry(geom)
                layer_response.commitChanges()

            #Correction des géométries (déformations causées par les changements de projection)

            alg_params = {
                    'INPUT': file_path_shp,
                    'METHOD': 1,  
                    'OUTPUT': file_path_shp
                }
            try:
                processing.run('native:fixgeometries', alg_params)
            except Exception:
                fix_layer = QgsVectorLayer(file_path_shp, f'RPG_{i}', 'ogr') #L'ancien shapefile sera écrasé
                
            if not fix_layer.isValid():
                self.appendConsoleMessage(f"Erreur : Impossible de charger la couche RPG_{i}")
            else:
                pr = fix_layer.dataProvider() #Accès aux données 
                fix_layer.startEditing()
                pr.addAttributes([QgsField("Annee", QVariant.Date)]) #Ajout d'une colonne date
                fix_layer.updateFields()
                index=pr.fieldNameIndex("Annee")

                #Ajout des données "date" par année de la couche
                features = fix_layer.getFeatures()
                for feature in features:
                    feature['Annee'] = str(i) +"-01-01"
                    fix_layer.updateFeature(feature)
                fix_layer.commitChanges()
                self.appendConsoleMessage("Couche modifiée")

                #Renommage de tous les champs avec des majuscules (adaptation aux RPGs habituellement en majuscule)
                pr = fix_layer.dataProvider()
                fix_layer.startEditing()
                for field in fix_layer.fields():
                    if field.name() != field.name().upper():
                        #Mise en majuscule des noms des champs (pour convenir aux besoins de l'algorithme de traitement de données vectorielles de Geofield 
                        #adapté pour les données téléchargeables hors API)
                        fix_layer.renameAttribute(fix_layer.fields().indexFromName(field.name()), field.name().upper()) 
                fix_layer.updateFeature(feature)
                fix_layer.commitChanges()

                QgsProject.instance().addMapLayer(fix_layer)
            
            #Activation des paramètres temporels si l'utilisateur souhaite utiliser le contrôleur temporel de QGIS
            temp_properties = fix_layer.temporalProperties()
            temp_properties.setIsActive(True)
            temp_properties.setStartField('Annee')  # Champ contenant les dates
            temp_properties.setMode(QgsVectorLayerTemporalProperties.ModeFeatureDateTimeInstantFromField)
            self.appendConsoleMessage("Paramètres temporels activés")


        #Mise en place de la projection finale en UTM zone et rafraichissement du projet en cours
        QgsProject.instance().setCrs(new_proj_)
        iface.mapCanvas().refresh

    def appendConsoleMessage(self, message): #Système de message pour le plugin 
        """
        Affichage des messages à l'utilisateur dans la console du plugin
        """
        cur_text = self.text_edit_console.toPlainText() #Récupération du texte
        if cur_text: 
            self.text_edit_console.setPlainText(cur_text + f"\n{message}") # Ajout d'un massage au texte actuel avec un caractère de nouvelle ligne le séparant
        else:
            self.text_edit_console.setPlainText(message)

if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = DataDownloadTool()  
    window.show()
    sys.exit(app.exec_())
