<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis readOnly="0" styleCategories="AllStyleCategories" maxScale="0" minScale="100000000" hasScaleBasedVisibilityFlag="0" labelsEnabled="0" simplifyMaxScale="1" symbologyReferenceScale="-1" simplifyLocal="1" simplifyDrawingTol="1" simplifyDrawingHints="1" version="3.32.3-Lima" simplifyAlgorithm="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal mode="0" endField="" durationField="" startField="" accumulate="0" fixedDuration="0" startExpression="" enabled="0" endExpression="" durationUnit="min" limitMode="0">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <elevation zoffset="0" binding="Centroid" type="IndividualFeatures" symbology="Line" showMarkerSymbolInSurfacePlots="0" respectLayerSymbol="1" zscale="1" extrusion="0" extrusionEnabled="0" clamping="Terrain">
    <data-defined-properties>
      <Option type="Map">
        <Option type="QString" value="" name="name"/>
        <Option name="properties"/>
        <Option type="QString" value="collection" name="type"/>
      </Option>
    </data-defined-properties>
    <profileLineSymbol>
      <symbol is_animated="0" type="line" alpha="1" frame_rate="10" clip_to_extent="1" force_rhr="0" name="">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer locked="0" class="SimpleLine" id="{47ee1600-bbad-42d9-b56d-b5974451d4c0}" enabled="1" pass="0">
          <Option type="Map">
            <Option type="QString" value="0" name="align_dash_pattern"/>
            <Option type="QString" value="square" name="capstyle"/>
            <Option type="QString" value="5;2" name="customdash"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="customdash_map_unit_scale"/>
            <Option type="QString" value="MM" name="customdash_unit"/>
            <Option type="QString" value="0" name="dash_pattern_offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="dash_pattern_offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="dash_pattern_offset_unit"/>
            <Option type="QString" value="0" name="draw_inside_polygon"/>
            <Option type="QString" value="bevel" name="joinstyle"/>
            <Option type="QString" value="190,178,151,255" name="line_color"/>
            <Option type="QString" value="solid" name="line_style"/>
            <Option type="QString" value="0.6" name="line_width"/>
            <Option type="QString" value="MM" name="line_width_unit"/>
            <Option type="QString" value="0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="0" name="ring_filter"/>
            <Option type="QString" value="0" name="trim_distance_end"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_end_map_unit_scale"/>
            <Option type="QString" value="MM" name="trim_distance_end_unit"/>
            <Option type="QString" value="0" name="trim_distance_start"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_start_map_unit_scale"/>
            <Option type="QString" value="MM" name="trim_distance_start_unit"/>
            <Option type="QString" value="0" name="tweak_dash_pattern_on_corners"/>
            <Option type="QString" value="0" name="use_custom_dash"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="width_map_unit_scale"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileLineSymbol>
    <profileFillSymbol>
      <symbol is_animated="0" type="fill" alpha="1" frame_rate="10" clip_to_extent="1" force_rhr="0" name="">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer locked="0" class="SimpleFill" id="{e30bdb20-130d-42c0-9c1a-de050d8ca16e}" enabled="1" pass="0">
          <Option type="Map">
            <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
            <Option type="QString" value="190,178,151,255" name="color"/>
            <Option type="QString" value="bevel" name="joinstyle"/>
            <Option type="QString" value="0,0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="136,127,108,255" name="outline_color"/>
            <Option type="QString" value="solid" name="outline_style"/>
            <Option type="QString" value="0.2" name="outline_width"/>
            <Option type="QString" value="MM" name="outline_width_unit"/>
            <Option type="QString" value="solid" name="style"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileFillSymbol>
    <profileMarkerSymbol>
      <symbol is_animated="0" type="marker" alpha="1" frame_rate="10" clip_to_extent="1" force_rhr="0" name="">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer locked="0" class="SimpleMarker" id="{171a562e-36bc-46b7-b2db-5ff699dcc723}" enabled="1" pass="0">
          <Option type="Map">
            <Option type="QString" value="0" name="angle"/>
            <Option type="QString" value="square" name="cap_style"/>
            <Option type="QString" value="190,178,151,255" name="color"/>
            <Option type="QString" value="1" name="horizontal_anchor_point"/>
            <Option type="QString" value="bevel" name="joinstyle"/>
            <Option type="QString" value="diamond" name="name"/>
            <Option type="QString" value="0,0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="136,127,108,255" name="outline_color"/>
            <Option type="QString" value="solid" name="outline_style"/>
            <Option type="QString" value="0.2" name="outline_width"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale"/>
            <Option type="QString" value="MM" name="outline_width_unit"/>
            <Option type="QString" value="diameter" name="scale_method"/>
            <Option type="QString" value="3" name="size"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="size_map_unit_scale"/>
            <Option type="QString" value="MM" name="size_unit"/>
            <Option type="QString" value="1" name="vertical_anchor_point"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileMarkerSymbol>
  </elevation>
  <renderer-v2 forceraster="0" type="graduatedSymbol" attr="nbChangeC" graduatedMethod="GraduatedColor" symbollevels="0" enableorderby="0" referencescale="-1">
    <ranges>
      <range lower="0.000000000000000" symbol="0" upper="1.000000000000000" label="Pas de rotation culturale" render="true"/>
      <range lower="1.000000000000000" symbol="1" upper="2.000000000000000" label="Rotation culturale" render="true"/>
    </ranges>
    <symbols>
      <symbol is_animated="0" type="fill" alpha="1" frame_rate="10" clip_to_extent="1" force_rhr="0" name="0">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer locked="0" class="SimpleFill" id="{fd38e7e8-af15-41ab-9c51-e51e41a538e4}" enabled="1" pass="0">
          <Option type="Map">
            <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
            <Option type="QString" value="255,255,255,255" name="color"/>
            <Option type="QString" value="bevel" name="joinstyle"/>
            <Option type="QString" value="0,0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="35,35,35,255" name="outline_color"/>
            <Option type="QString" value="solid" name="outline_style"/>
            <Option type="QString" value="0.26" name="outline_width"/>
            <Option type="QString" value="MM" name="outline_width_unit"/>
            <Option type="QString" value="solid" name="style"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol is_animated="0" type="fill" alpha="1" frame_rate="10" clip_to_extent="1" force_rhr="0" name="1">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer locked="0" class="SimpleFill" id="{fd38e7e8-af15-41ab-9c51-e51e41a538e4}" enabled="1" pass="0">
          <Option type="Map">
            <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
            <Option type="QString" value="255,0,0,255" name="color"/>
            <Option type="QString" value="bevel" name="joinstyle"/>
            <Option type="QString" value="0,0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="35,35,35,255" name="outline_color"/>
            <Option type="QString" value="solid" name="outline_style"/>
            <Option type="QString" value="0.26" name="outline_width"/>
            <Option type="QString" value="MM" name="outline_width_unit"/>
            <Option type="QString" value="solid" name="style"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <source-symbol>
      <symbol is_animated="0" type="fill" alpha="1" frame_rate="10" clip_to_extent="1" force_rhr="0" name="0">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer locked="0" class="SimpleFill" id="{fd38e7e8-af15-41ab-9c51-e51e41a538e4}" enabled="1" pass="0">
          <Option type="Map">
            <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
            <Option type="QString" value="196,60,57,255" name="color"/>
            <Option type="QString" value="bevel" name="joinstyle"/>
            <Option type="QString" value="0,0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="35,35,35,255" name="outline_color"/>
            <Option type="QString" value="solid" name="outline_style"/>
            <Option type="QString" value="0.26" name="outline_width"/>
            <Option type="QString" value="MM" name="outline_width_unit"/>
            <Option type="QString" value="solid" name="style"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </source-symbol>
    <colorramp type="gradient" name="[source]">
      <Option type="Map">
        <Option type="QString" value="255,255,255,255" name="color1"/>
        <Option type="QString" value="255,0,0,255" name="color2"/>
        <Option type="QString" value="ccw" name="direction"/>
        <Option type="QString" value="0" name="discrete"/>
        <Option type="QString" value="gradient" name="rampType"/>
        <Option type="QString" value="rgb" name="spec"/>
      </Option>
    </colorramp>
    <classificationMethod id="Fixed">
      <symmetricMode symmetrypoint="0" astride="0" enabled="0"/>
      <labelFormat format="%1 - %2" trimtrailingzeroes="1" labelprecision="1"/>
      <parameters>
        <Option type="Map">
          <Option type="double" value="1" name="INTERVAL"/>
        </Option>
      </parameters>
      <extraInformation/>
    </classificationMethod>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <customproperties>
    <Option type="Map">
      <Option type="List" name="dualview/previewExpressions">
        <Option type="QString" value="&quot;CODE_CULTU&quot;"/>
      </Option>
      <Option type="int" value="0" name="embeddedWidgets/count"/>
      <Option name="variableNames"/>
      <Option name="variableValues"/>
    </Option>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory spacing="5" spacingUnitScale="3x:0,0,0,0,0,0" labelPlacementMethod="XHeight" showAxis="1" scaleBasedVisibility="0" sizeScale="3x:0,0,0,0,0,0" sizeType="MM" minimumSize="0" backgroundAlpha="255" lineSizeScale="3x:0,0,0,0,0,0" minScaleDenominator="0" enabled="0" width="15" opacity="1" spacingUnit="MM" penColor="#000000" barWidth="5" diagramOrientation="Up" penAlpha="255" backgroundColor="#ffffff" height="15" maxScaleDenominator="1e+08" penWidth="0" rotationOffset="270" direction="0" lineSizeType="MM" scaleDependency="Area">
      <fontProperties style="" description="MS Shell Dlg 2,7.8,-1,5,50,0,0,0,0,0" strikethrough="0" italic="0" underline="0" bold="0"/>
      <axisSymbol>
        <symbol is_animated="0" type="line" alpha="1" frame_rate="10" clip_to_extent="1" force_rhr="0" name="">
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <layer locked="0" class="SimpleLine" id="{5eb50f71-b594-420f-913f-92523fa2de72}" enabled="1" pass="0">
            <Option type="Map">
              <Option type="QString" value="0" name="align_dash_pattern"/>
              <Option type="QString" value="square" name="capstyle"/>
              <Option type="QString" value="5;2" name="customdash"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="customdash_map_unit_scale"/>
              <Option type="QString" value="MM" name="customdash_unit"/>
              <Option type="QString" value="0" name="dash_pattern_offset"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="dash_pattern_offset_map_unit_scale"/>
              <Option type="QString" value="MM" name="dash_pattern_offset_unit"/>
              <Option type="QString" value="0" name="draw_inside_polygon"/>
              <Option type="QString" value="bevel" name="joinstyle"/>
              <Option type="QString" value="35,35,35,255" name="line_color"/>
              <Option type="QString" value="solid" name="line_style"/>
              <Option type="QString" value="0.26" name="line_width"/>
              <Option type="QString" value="MM" name="line_width_unit"/>
              <Option type="QString" value="0" name="offset"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
              <Option type="QString" value="MM" name="offset_unit"/>
              <Option type="QString" value="0" name="ring_filter"/>
              <Option type="QString" value="0" name="trim_distance_end"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_end_map_unit_scale"/>
              <Option type="QString" value="MM" name="trim_distance_end_unit"/>
              <Option type="QString" value="0" name="trim_distance_start"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_start_map_unit_scale"/>
              <Option type="QString" value="MM" name="trim_distance_start_unit"/>
              <Option type="QString" value="0" name="tweak_dash_pattern_on_corners"/>
              <Option type="QString" value="0" name="use_custom_dash"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="width_map_unit_scale"/>
            </Option>
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings zIndex="0" placement="1" dist="0" linePlacementFlags="18" priority="0" obstacle="0" showAll="1">
    <properties>
      <Option type="Map">
        <Option type="QString" value="" name="name"/>
        <Option name="properties"/>
        <Option type="QString" value="collection" name="type"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration type="Map">
      <Option type="Map" name="QgsGeometryGapCheck">
        <Option type="double" value="0" name="allowedGapsBuffer"/>
        <Option type="bool" value="false" name="allowedGapsEnabled"/>
        <Option type="QString" value="" name="allowedGapsLayer"/>
      </Option>
    </checkConfiguration>
  </geometryOptions>
  <legend type="default-vector" showLabelLegend="0"/>
  <referencedLayers/>
  <fieldConfiguration>
    <field configurationFlags="None" name="SURF_PARC">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="CODE_CULTU">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="CODE_GROUP">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="nbChangeC">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="diff_cultu">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="nbChangeG">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="diff_group">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="mean_0">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="Couvert_hi">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias index="0" field="SURF_PARC" name=""/>
    <alias index="1" field="CODE_CULTU" name=""/>
    <alias index="2" field="CODE_GROUP" name=""/>
    <alias index="3" field="nbChangeC" name=""/>
    <alias index="4" field="diff_cultu" name=""/>
    <alias index="5" field="nbChangeG" name=""/>
    <alias index="6" field="diff_group" name=""/>
    <alias index="7" field="mean_0" name=""/>
    <alias index="8" field="Couvert_hi" name=""/>
  </aliases>
  <splitPolicies>
    <policy field="SURF_PARC" policy="Duplicate"/>
    <policy field="CODE_CULTU" policy="Duplicate"/>
    <policy field="CODE_GROUP" policy="Duplicate"/>
    <policy field="nbChangeC" policy="Duplicate"/>
    <policy field="diff_cultu" policy="Duplicate"/>
    <policy field="nbChangeG" policy="Duplicate"/>
    <policy field="diff_group" policy="Duplicate"/>
    <policy field="mean_0" policy="Duplicate"/>
    <policy field="Couvert_hi" policy="Duplicate"/>
  </splitPolicies>
  <defaults>
    <default applyOnUpdate="0" field="SURF_PARC" expression=""/>
    <default applyOnUpdate="0" field="CODE_CULTU" expression=""/>
    <default applyOnUpdate="0" field="CODE_GROUP" expression=""/>
    <default applyOnUpdate="0" field="nbChangeC" expression=""/>
    <default applyOnUpdate="0" field="diff_cultu" expression=""/>
    <default applyOnUpdate="0" field="nbChangeG" expression=""/>
    <default applyOnUpdate="0" field="diff_group" expression=""/>
    <default applyOnUpdate="0" field="mean_0" expression=""/>
    <default applyOnUpdate="0" field="Couvert_hi" expression=""/>
  </defaults>
  <constraints>
    <constraint notnull_strength="0" exp_strength="0" constraints="0" unique_strength="0" field="SURF_PARC"/>
    <constraint notnull_strength="0" exp_strength="0" constraints="0" unique_strength="0" field="CODE_CULTU"/>
    <constraint notnull_strength="0" exp_strength="0" constraints="0" unique_strength="0" field="CODE_GROUP"/>
    <constraint notnull_strength="0" exp_strength="0" constraints="0" unique_strength="0" field="nbChangeC"/>
    <constraint notnull_strength="0" exp_strength="0" constraints="0" unique_strength="0" field="diff_cultu"/>
    <constraint notnull_strength="0" exp_strength="0" constraints="0" unique_strength="0" field="nbChangeG"/>
    <constraint notnull_strength="0" exp_strength="0" constraints="0" unique_strength="0" field="diff_group"/>
    <constraint notnull_strength="0" exp_strength="0" constraints="0" unique_strength="0" field="mean_0"/>
    <constraint notnull_strength="0" exp_strength="0" constraints="0" unique_strength="0" field="Couvert_hi"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" exp="" field="SURF_PARC"/>
    <constraint desc="" exp="" field="CODE_CULTU"/>
    <constraint desc="" exp="" field="CODE_GROUP"/>
    <constraint desc="" exp="" field="nbChangeC"/>
    <constraint desc="" exp="" field="diff_cultu"/>
    <constraint desc="" exp="" field="nbChangeG"/>
    <constraint desc="" exp="" field="diff_group"/>
    <constraint desc="" exp="" field="mean_0"/>
    <constraint desc="" exp="" field="Couvert_hi"/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
  </attributeactions>
  <attributetableconfig actionWidgetStyle="dropDown" sortExpression="&quot;Couvert_hi&quot;" sortOrder="0">
    <columns>
      <column type="field" width="-1" hidden="0" name="SURF_PARC"/>
      <column type="field" width="-1" hidden="0" name="CODE_CULTU"/>
      <column type="field" width="-1" hidden="0" name="CODE_GROUP"/>
      <column type="field" width="-1" hidden="0" name="nbChangeC"/>
      <column type="field" width="153" hidden="0" name="diff_cultu"/>
      <column type="field" width="-1" hidden="0" name="nbChangeG"/>
      <column type="field" width="-1" hidden="0" name="diff_group"/>
      <column type="field" width="-1" hidden="0" name="mean_0"/>
      <column type="field" width="-1" hidden="0" name="Couvert_hi"/>
      <column type="actions" width="-1" hidden="1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui est appelée lorsque le formulaire est
ouvert.

Utilisez cette fonction pour ajouter une logique supplémentaire à vos formulaires.

Entrez le nom de la fonction dans le champ 
"Fonction d'initialisation Python".
Voici un exemple:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field editable="1" name="CODE_CULTU"/>
    <field editable="1" name="CODE_GROUP"/>
    <field editable="1" name="Couvert_hi"/>
    <field editable="1" name="SURF_PARC"/>
    <field editable="1" name="diff_cultu"/>
    <field editable="1" name="diff_group"/>
    <field editable="1" name="mean_0"/>
    <field editable="1" name="nbChangeC"/>
    <field editable="1" name="nbChangeG"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="CODE_CULTU"/>
    <field labelOnTop="0" name="CODE_GROUP"/>
    <field labelOnTop="0" name="Couvert_hi"/>
    <field labelOnTop="0" name="SURF_PARC"/>
    <field labelOnTop="0" name="diff_cultu"/>
    <field labelOnTop="0" name="diff_group"/>
    <field labelOnTop="0" name="mean_0"/>
    <field labelOnTop="0" name="nbChangeC"/>
    <field labelOnTop="0" name="nbChangeG"/>
  </labelOnTop>
  <reuseLastValue>
    <field reuseLastValue="0" name="CODE_CULTU"/>
    <field reuseLastValue="0" name="CODE_GROUP"/>
    <field reuseLastValue="0" name="Couvert_hi"/>
    <field reuseLastValue="0" name="SURF_PARC"/>
    <field reuseLastValue="0" name="diff_cultu"/>
    <field reuseLastValue="0" name="diff_group"/>
    <field reuseLastValue="0" name="mean_0"/>
    <field reuseLastValue="0" name="nbChangeC"/>
    <field reuseLastValue="0" name="nbChangeG"/>
  </reuseLastValue>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression>"CODE_CULTU"</previewExpression>
  <mapTip enabled="1"></mapTip>
  <layerGeometryType>2</layerGeometryType>
</qgis>
