"""
Ce programme python est une transcription du modeleur 'RPG_enrichi_modeleur.model3'
présent dans le projet Gitlab. Ce programme python est uniquement fonctionnel en
étant lancé depuis la console python de QGIS.
"""

"""
Import des libraries QGIS

"""
from qgis.core import (QgsProcessing,
                       QgsProcessingAlgorithm,
                       QgsProcessingMultiStepFeedback,
                       QgsProcessingParameterFeatureSource,
                       QgsProcessingParameterFeatureSink,
                       QgsCoordinateReferenceSystem)
import os
import processing

"""Création d'une classe 'RPG' qui détermine les paramètres propres au modeleur QGIS"""
class Rpg(QgsProcessingAlgorithm):

    """ La fonction 'initAlgorithm' permet de définir les paramètres en entrée et
    en sortie du modeleur ou de ses fonctions intermédiaires"""
    def initAlgorithm(self, config=None):
        #paramètres en entrée du modèle : 2 couches vecteurs (RPG)
        self.addParameter(QgsProcessingParameterFeatureSource('donne_rpg_1re_anne', 'donnée RPG 1ère année', types=[QgsProcessing.TypeVectorPolygon], defaultValue=None))
        self.addParameter(QgsProcessingParameterFeatureSource('donne_rpg_2me_anne', 'donnée RPG 2ème année', types=[QgsProcessing.TypeVectorPolygon], defaultValue=None))
        #paramètre en sortie : 1 couche vecteur (RPG amélioré)
        self.addParameter(QgsProcessingParameterFeatureSink('RPG_amelioreshp', 'RPG_ameliore.shp', type=QgsProcessing.TypeVectorAnyGeometry, createByDefault=True, supportsAppend=True, defaultValue=None))

    """La fonction 'processAlgorithm' exécute les fonctions de l'algorithme dans
    l'ordre définit lors de la construction du modeleur"""
    def processAlgorithm(self, parameters, context, model_feedback):
        # la variable 'feedback' permet de synchroniser les fonctions intermédiaires
        # entre elles afin d'assurer la bonne gestion des entrées et sorties de chaque fonction intermédiaire"""
        feedback = QgsProcessingMultiStepFeedback(33, model_feedback)
        results = {} # dictionnaire contenant la couche finale
        outputs = {} # dictionnaire contenant les sorties des fonctions utilisées

        '''
        entrées : - alg_id = le nom de l'algorithme Qgis,
                  - alg_params = les paramètres de la fonction Qgis
                  - feedback = la variable
                  - context = le paramètre en entrée de processAlgorithm
        sortie : la couche créée / mise à jour par la fonction Qgis
        '''
        def run_processing_algorithm(alg_id, alg_params, feedback, context):
            return processing.run(alg_id, alg_params, context=context, feedback=feedback, is_child_algorithm=True)
        
        '''
        entrées : - input_layer = l'identifiant d'une couche vectorielle
                  - expression = une chaine de caractères qui décrit les conditions
                  d'extraction des entités de la couche
        sortie : les entités extraites  
        '''
        def extract_by_expression(input_layer, expression):
            alg_params = {'EXPRESSION': expression, 'INPUT': input_layer, 'OUTPUT': QgsProcessing.TEMPORARY_OUTPUT, 'FAIL_OUTPUT': QgsProcessing.TEMPORARY_OUTPUT}
            result = run_processing_algorithm('native:extractbyexpression', alg_params, feedback, context)
            if result and 'FAIL_OUTPUT' in result:
                return result
            else:
                return {'OUTPUT': result['OUTPUT'], 'FAIL_OUTPUT': None}

        '''
        entrées : - input layer = l'identifiant d'une couche vectorielle
                  - columns = la liste des noms de colonnes à supprimer
        sortie : la couche sans les colonnes de columns  
        '''
        def delete_columns(input_layer, columns):
            alg_params = {'COLUMN': columns, 'INPUT': input_layer, 'OUTPUT': QgsProcessing.TEMPORARY_OUTPUT}
            return run_processing_algorithm('native:deletecolumn', alg_params, feedback, context)

        '''
        entrées : - input_layer = l'identifiant d'une couche vectorielle
                  - method = un entier qui fait référence à la méthode utilisée pour
                  réparer les couches
                  - output = la sortie prévue. Par défaut, une couche temporaire,
                  comme "réparer une couche" est la dernière opération de l'algorithme,
                  peut contenir "result"
        sortie : les entités extraites  
        '''
        def repare_geometry(input_layer, method, output = QgsProcessing.TEMPORARY_OUTPUT):
            alg_params = {'INPUT': input_layer, 'METHOD' : method, 'OUTPUT': output}
            return run_processing_algorithm('native:fixgeometries', alg_params, feedback, context)

        '''
        entrées : - input_layer = l'identifiant d'une couche vectorielle
                  - over_layer = l'identifiant de la couche superposée
        sortie : la couches des intersections des deux couches en entrée 
        '''
        def intersection_multiple(input_layer, overlay_layer):
            alg_params = {'INPUT': input_layer, 'OVERLAY' : overlay_layer, 'OVERLAY_FIELDS_PREFIX': '', 'OUTPUT': QgsProcessing.TEMPORARY_OUTPUT}
            return run_processing_algorithm('native:intersection', alg_params, feedback, context)

        '''
        entrées : - input_layer = l'identifiant d'une couche vectorielle
                  - over_layer = l'identifiant de la couche masque
        sortie : les parcelles / parties de parcelles d'input layer qui
                ne sont pas dans over_layer
        '''
        def difference(input_layer, overlay_layer):
            alg_params = {'INPUT': input_layer, 'OVERLAY' : overlay_layer, 'OUTPUT': QgsProcessing.TEMPORARY_OUTPUT}
            return run_processing_algorithm('native:difference', alg_params, feedback, context)
        
        '''
        entrées : - input_layer = l'identifiant d'une couche vectorielle
                  - name = le nom du champs (colonne) à créer ou modifier
                  - formula = chaine de carractère qui décrit comment remplir le champs
                  - fied_type = un entier décrivant le type du champs
                  - length = entier, nombre maximal d'éléments (caractères ou numériques)
                  - precision = pour les décimaux, nombre de chiffres significatifs
                  - new_field = booleen, dit s'il s'agit d'une update ou d'un nouveau
                    champs. Faux par défaut
        sortie : la couches mise à jour
        '''
        def field_calculator(input_layer, name, formula, field_type, length, precision, new = False):
            #cond = f'{name} IS NULL'
            #alg_params = {'FIELD_LENGTH': length, 'FIELD_NAME': name, 'FIELD_PRECISION': precision, 'FIELD_TYPE': field_type, 'FORMULA': formula, 'INPUT': input_layer, 'NEW_FIELD': cond, 'OUTPUT': QgsProcessing.TEMPORARY_OUTPUT}
            alg_params = {'FIELD_LENGTH': length, 'FIELD_NAME': name, 'FIELD_PRECISION': precision, 'FIELD_TYPE': field_type, 'FORMULA': formula, 'INPUT': input_layer, 'NEW_FIELD': new, 'OUTPUT': QgsProcessing.TEMPORARY_OUTPUT}
            return run_processing_algorithm('native:fieldcalculator', alg_params, feedback, context)

        '''
        entrées : - layers = la liste des vecteurs à fusionner
        sortie : les vecteurs fusionnés
        '''
        def merge_vector_layers(layers):
            alg_params = {'CRS': None, 'LAYERS': layers, 'OUTPUT': QgsProcessing.TEMPORARY_OUTPUT}
            return run_processing_algorithm('native:mergevectorlayers', alg_params, feedback, context)
        
        '''
        entrées : - input_layer = la couche à reprojeter
                  - target_crs = le scr dans lequel on veut la couche.
                    Dans ce projet, il s'agit du lambert 93 car les parcelles considérées
                    sont en France (la valeur par défaut n'est jamais modifiée). Le scr est
                    néanmoins un paramètre de la fonction "reprojection" pour qu'il puisse
                    être adapté à d'autres régions.
        sortie : la couche reprojetée
        '''
        def reprojection(input_layer, target_crs = QgsCoordinateReferenceSystem('IGNF:LAMB93')):
            alg_params = { 'INPUT': input_layer, 'OPERATION': '', 'TARGET_CRS': target_crs, 'OUTPUT': QgsProcessing.TEMPORARY_OUTPUT}
            return processing.run('native:reprojectlayer', alg_params, context=context, feedback=feedback, is_child_algorithm=True)

        # Reprojeter les couches en entrée
        outputs['ReprojeterUneCouche_1'] = reprojection(parameters['donne_rpg_1re_anne'])
        outputs['ReprojeterUneCouche_2'] = reprojection(parameters['donne_rpg_2me_anne'])
        
        # Extraire par expression
        expr = 'NOT("CODE_CULTU"=\'BR\' OR "CODE_CULTU"=\'EL\' OR "CODE_CULTU"=\'BOR\' OR "CODE_CULTU"=\'SNA\' OR "CODE_CULTU"=\'BTA\'OR "CODE_CULTU"=\'B168\'OR "CODE_CULTU"=\'B169\')'
        outputs['ExtractByExpression_1'] = extract_by_expression(outputs['ReprojeterUneCouche_1']['OUTPUT'], expr)
        outputs['ExtractByExpression_2'] = extract_by_expression(outputs['ReprojeterUneCouche_2']['OUTPUT'], expr)

        # Supprimer champ(s)
        columns = ['ID_PARCEL','fid','CULTURE_D1','CULTURE_D2','diff','CODE_GR_NU']
        outputs['SupprimerChamps_1'] = delete_columns(outputs['ExtractByExpression_1']['OUTPUT'], columns)
        outputs['SupprimerChamps_2'] = delete_columns(outputs['ExtractByExpression_2']['OUTPUT'], columns)
        
        # Réparer les géométries
        outputs['RparerLesGomtries_1'] = repare_geometry(outputs['SupprimerChamps_1']['OUTPUT'], 1)
        outputs['RparerLesGomtries_2'] = repare_geometry(outputs['SupprimerChamps_2']['OUTPUT'], 1)
        
        # Intersection (multiple)
        outputs['IntersectionMultiple'] = intersection_multiple(outputs['RparerLesGomtries_1']['OUTPUT'], outputs['RparerLesGomtries_1']['OUTPUT'])
        
        # Réparer les géométries
        outputs['RparerLesGomtries_3'] = repare_geometry(outputs['IntersectionMultiple']['OUTPUT'], 1)

        # Différences
        outputs['Diffrence_1'] = difference(outputs['RparerLesGomtries_1']['OUTPUT'], outputs['RparerLesGomtries_3']['OUTPUT'])
        outputs['Diffrence_2'] = difference(outputs['RparerLesGomtries_2']['OUTPUT'], outputs['RparerLesGomtries_3']['OUTPUT'])
        
        # Calculatrice de champ
        outputs['CalculatriceDeChamp_1'] = field_calculator(outputs['Diffrence_2']['OUTPUT'], 'SURF_PARC_2', '"SURF_PARC"', 0, 15, 2,True)
        outputs['CalculatriceDeChamp_2'] = field_calculator(outputs['CalculatriceDeChamp_1']['OUTPUT'], 'CODE_GROUP_2', '"CODE_GROUP"', 2, 2, 0,True)
        outputs['CalculatriceDeChamp_3'] = field_calculator(outputs['CalculatriceDeChamp_2']['OUTPUT'], 'CODE_CULTU_2', '"CODE_CULTU"', 2, 4, 0,True)

        # Supprimer champ(s)
        columns = ['SURF_PARC','CODE_CULTU','CODE_GROUP']
        outputs['SupprimerChamps_3'] = delete_columns(outputs['CalculatriceDeChamp_3']['OUTPUT'], columns)

        # Fusionner des couches vecteur
        layers = [outputs['Diffrence_1']['OUTPUT'],outputs['SupprimerChamps_3']['OUTPUT'],outputs['RparerLesGomtries_3']['OUTPUT']]
        outputs['FusionnerDesCouchesVecteur'] = merge_vector_layers(layers)
    
        # Réparer les géométries
        outputs['RparerLesGomtries_4'] = repare_geometry(outputs['FusionnerDesCouchesVecteur']['OUTPUT'], 1)
        
        # Extraire par expression
        expr = '( max ( area($geometry) * 10^(-2) / "SURF_PARC", area($geometry) * 10^(-2) / "SURF_PARC_2") > 5 or "SURF_PARC" IS NULL and "SURF_PARC_2" IS NULL and area($geometry)> 500) and geometry_type($geometry) = \'Polygon\''
        outputs['ExtractByExpression_3'] = extract_by_expression(outputs['RparerLesGomtries_4']['OUTPUT'], expr)

        # Calculatrice de champ
        formula = 'if("CODE_CULTU" IS NULL, \'FND\', "CODE_CULTU")\r\n'
        outputs['CalculatriceDeChamp_4'] = field_calculator(outputs['ExtractByExpression_3']['OUTPUT'], 'CODE_CULTU', formula, 2, 4, 0)
        
        formula = 'if("CODE_GROUP" IS NULL,\'5\',"CODE_GROUP")'
        outputs['CalculatriceDeChamp_5'] = field_calculator(outputs['CalculatriceDeChamp_4']['OUTPUT'], 'CODE_GROUP', formula, 2, 2, 0)
        
        formula = 'if("CODE_CULTU_2" IS NULL, \'FND\', "CODE_CULTU_2")'
        outputs['CalculatriceDeChamp_6'] = field_calculator(outputs['CalculatriceDeChamp_5']['OUTPUT'], 'CODE_CULTU_2', formula, 2, 4, 0)
        
        formula = 'if("CODE_GROUP_2" IS NULL, \'5\', "CODE_GROUP_2")'
        outputs['CalculatriceDeChamp_7'] = field_calculator(outputs['CalculatriceDeChamp_6']['OUTPUT'], 'CODE_GROUP_2', formula, 2, 2, 0)
        
        # calcul des indices
        formula = 'if("CODE_CULTU" = "CODE_CULTU_2",0,1)'
        outputs['CalculatriceDeChamp_8'] = field_calculator(outputs['CalculatriceDeChamp_7']['OUTPUT'], 'diff', formula, 1, 1, 0)
        
        formula = 'if("nbChangeC" IS NULL, "diff","nbChangeC" + "diff")'
        outputs['CalculatriceDeChamp_9'] = field_calculator(outputs['CalculatriceDeChamp_8']['OUTPUT'], 'nbChangeC', formula, 1, 3, 0)
        
        formula = 'CASE WHEN "diff_cultu" IS NULL THEN "CODE_CULTU"+\'->\'+"CODE_CULTU_2" ELSE "diff_cultu"+\'->\'+"CODE_CULTU_2" END'
        outputs['CalculatriceDeChamp_10'] = field_calculator(outputs['CalculatriceDeChamp_9']['OUTPUT'], 'diff_cultu', formula, 2, 80, 0)
        
        formula = 'if("nbChangeG" IS NULL, if("CODE_GROUP"="CODE_GROUP_2",0,1),if("CODE_GROUP"="CODE_GROUP_2","nbChangeG", "nbChangeG" + 1))'
        outputs['CalculatriceDeChamp_11'] = field_calculator(outputs['CalculatriceDeChamp_10']['OUTPUT'], 'nbChangeG', formula, 1, 3, 0)
        
        formula = 'CASE WHEN "diff_group" IS NULL THEN "CODE_GROUP"+\'->\'+"CODE_GROUP_2" ELSE "diff_group"+\'->\'+"CODE_GROUP_2" END'
        outputs['CalculatriceDeChamp_12'] = field_calculator(outputs['CalculatriceDeChamp_11']['OUTPUT'], 'diff_group', formula, 2, 80, 0)
        
        # changements de valeurs : celles du RPG de la deuxième année remplace
        # celles de la première année
        formula = 'if("SURF_PARC_2" IS NOT NULL,"SURF_PARC_2", if("SURF_PARC" IS NOT NULL, "SURF_PARC", area($geometry)*10^(-4)))'
        outputs['CalculatriceDeChamp_13'] = field_calculator(outputs['CalculatriceDeChamp_12']['OUTPUT'], 'SURF_PARC', formula, 0, 15, 2)
        
        formula = '"CODE_CULTU_2"'
        outputs['CalculatriceDeChamp_14'] = field_calculator(outputs['CalculatriceDeChamp_13']['OUTPUT'], 'CODE_CULTU', formula, 2, 4, 0)
        
        formula = '"CODE_GROUP_2"'
        outputs['CalculatriceDeChamp_15'] = field_calculator(outputs['CalculatriceDeChamp_14']['OUTPUT'], 'CODE_GROUP', formula, 2, 2, 0)

        formula = 'to_int("CODE_GROUP")'
        outputs['CalculatriceDeChamp_16'] = field_calculator(outputs['CalculatriceDeChamp_15']['OUTPUT'], 'CODE_GR_NU', formula, 1, 2, 0)

        # Supprimer champ(s)
        columns = ['SURF_PARC_2','CODE_CULTU_2','CODE_GROUP_2',"nbChangeC_2","nbChangeG_2","diff_cultu_2","diff_group_2",'layer','path']
        outputs['SupprimerChamps_4'] = delete_columns(outputs['CalculatriceDeChamp_16']['OUTPUT'], columns)

        # Réparer les géométries
        results['RPG_amelioreshp'] = repare_geometry(outputs['SupprimerChamps_4']['OUTPUT'], 1, parameters['RPG_amelioreshp'])
        return results

    def name(self):
        return 'RPG'

    def displayName(self):
        return 'RPG'

    def group(self):
        return ''

    def groupId(self):
        return ''

    def createInstance(self):
        return Rpg()
